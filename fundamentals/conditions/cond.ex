# Cond is equivalent to else/if in imperative languages but is used way less in elixir.
cond do
  2 + 2 === 5 -> IO.puts "This will not be true"
  2 * 2 === 3 -> IO.puts "Nor this..."
  1 + 1 === 2 -> IO.puts "But this will"
  true -> IO.puts "If none of the conditions above matchs, this one will. Equivalent to else."
end