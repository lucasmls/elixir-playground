if true do
  IO.puts "Entered into if"
end

unless true do
  IO.puts "Will not enter here"
end

if false do
  IO.puts "Will not enter into if"
end

unless false do
  IO.puts "Entered in unless"
end