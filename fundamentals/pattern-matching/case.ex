case {1, 2, 3} do
  {4, 5, 6} -> IO.puts "This clause won't match"

  {1, x, 3} -> IO.puts "This clause will match and bind x to 2 in this clause. X: #{x}"

  _ -> IO.puts "This clause would match any value"
end

# The first clause here will only match if y is positive.
case {1, 2, 3} do
  {1, y, 3} when y > 0 -> IO.puts "Y is greater than 0. Y: #{y}"
  _ -> "Would match, if guard condition were not satisfied"
end
