{name, surname, age} = {"lucas", "mendes", 20}

IO.puts name
IO.puts surname
IO.puts age

[first_place, second_place, third_place] = ["Laisla", "Daniel", "Lucas"]

IO.puts first_place
IO.puts second_place
IO.puts third_place

[head | tail] = [1, 2, 3]
IO.inspect head
IO.inspect tail