IO.inspect Enum.map(
  [1, 2, 3, 4],
  fn x -> x * 2 end
)

IO.inspect Enum.map(
  %{1 => 2, 3 => 4},
  fn {k, v} -> k * v end
)

IO.inspect Enum.map(
  1..3,
  fn x -> x * 2 end
)