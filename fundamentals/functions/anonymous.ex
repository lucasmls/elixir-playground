af = fn
  x, y when x > 0 -> x + y
  x, y -> x * y
end

IO.puts af.(4, 10)
IO.puts af.(-2, 10)