# Compared to keyword lists, we can already see two differences:
#   Maps allow any value as a key.
#   Maps’ keys do not follow any ordering.

# Basic Usage
user = %{
  :name => "Lucas",
  :surname => "Mendes"
}

IO.puts user[:name]
IO.puts user[:surname]

# Pattern matching
%{} = %{:a => 1, 2 => :b} # Empty map matches all
%{:a => a, 2 => second} = %{:a => 1, 2 => :b}
IO.puts "a: (#{a}); second: (#{second})"

example_map = %{:name => "Lucas", :surname => "Mendes"}
IO.puts "Name: #{Map.get(example_map, :name)}"

# Variables can be used in map keys:
n = :name
IO.puts "Name using variable as key: #{example_map[n]}"

# Updating a key value (remember that we receives another map)
updated_map = %{example_map | :name => "Laisla"}
IO.puts "Updated name: #{updated_map[:name]}"

# Creating another key in the map (remember that we receives another map)
map_with_age = Map.put(example_map, :age, 20) # Remember that here, we're creating another map. So, we needed to pattern match in another variable.
IO.puts "Age: #{map_with_age[:age]}"

# When all the keys in a map are atoms, we can use the keyword syntax for convenience:
keyword_map = %{a: 1, b: 2}
IO.puts "Keyword map: #{keyword_map[:a]}"
IO.puts "Keyword map: #{keyword_map.b}"

# Nested maps
users = [
  lucas: %{name: "Lucas", age: 20, languages: ["Golang", "Elixir", "JavaScript"]},
  daniel: %{name: "Daniel", age: 21, languages: ["PHP", "Golang", "Elixir"]}
]

IO.inspect users

users = put_in users[:daniel].age, 22
IO.inspect users