# When we have a list of tuples, and the the first item of the tuple is an atom
# we call it a keyword list. Example above
list = [{:a, 1}, {:b, 2}]

IO.inspect list

IO.puts list[:a]
IO.puts list[:b]

# As we can see, we still have a list, so wen can use operators like ++ to add values into the keyword list
new_list = list ++ [{:c, 333}]
IO.puts new_list[:c]

# Keyword lists are important because they have three special characteristics:
  # Keys must be atoms.
  # Keys are ordered, as specified by the developer.
  # Keys can be given more than once.