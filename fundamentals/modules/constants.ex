defmodule Server do
  @initial_state %{host: "127.0.0.1", port: 8080}

  def print_module_state do
    IO.inspect @initial_state
  end

  @fn_data "Lucas Mendes"
  def first_data do
    @fn_data
  end

  @fn_data 13208
  def second_data, do: @fn_data
end

Server.print_module_state

IO.puts Server.first_data
IO.puts Server.second_data