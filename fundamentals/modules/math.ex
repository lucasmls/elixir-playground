# Besides the docs examples below, we have some others like:
# - @behaviour -> that is used for specifying an OTP or used-defined behaviour.
# - @before_compile -> that provides a hook that will be invoked before the module is compiled.

defmodule Math do
  @moduledoc """
  Provides math-related functions.
  ## Examples
    iex> Math.sum(1, 2)
    3
  """

  @doc """
  Calculates the sum of two numbers.
  """
  def sum(a, b) do
    do_sum(a, b)
  end

  defp do_sum(a, b) do
    a + b
  end

  def zero?(0) do
    true
  end

  def zero?(x) when is_integer(x) do
    false
  end

  def reduce([], acc) do
    acc
  end

  def reduce([head | tail], acc) do
    reduce(tail, head + acc)
  end

  def double_each([]) do
    []
  end

  def double_each([head | tail]) do
    [head * 2 | double_each(tail)]
  end
end

IO.puts Math.sum(1, 2)
IO.puts Math.zero?(0)
IO.puts Math.zero?(1)
# Reduce
IO.puts Math.reduce([1, 2, 3, 4, 5, 6], 0)
# Map
IO.inspect Math.double_each([1, 2, 3, 4, 5, 6])