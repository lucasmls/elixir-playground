defmodule DumbCalculator do
  @typedoc """
  A 4 digit year, e.g. 1984
  """
  @type year :: integer

  @type number_with_remark :: {number, String.t}

  @spec echo_year(year) :: integer
  def echo_year(year_to_echo) do
    year_to_echo
  end

  @spec add(number, number) :: number_with_remark
  def add(x, y), do: {x + y, "Do i need a calculator to do that?!"}

  @spec multiply(number, number) :: number_with_remark
  def multiply(x, y), do: {(x * y), "Come on man..."}
end

IO.inspect DumbCalculator.add(10, 10)
IO.inspect DumbCalculator.multiply(10, 10)