# send self(), {:hello, "world"}

receive do
  {:hello, message} -> IO.puts message
  {:world, _message} -> IO.puts "won't match"
  after
    1000 -> IO.puts "nothing receveid after 1s"
end

# When we spawn a process and they raises a error, our parent process will no know
# because the process are isolated. If we want to propagate to another process
# we would   need to spawn like so: 

# spawn_link fn -> raise "linked error, oops" end # will propagate the error
# spawn fn -> raise "linked error, oops" end # will not propagate

# We often link our process to supervisors which will detect when a processs dies
# and starts a new one in its place.