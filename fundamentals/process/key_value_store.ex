defmodule KeyValueStore do
  def start_link do
    Task.start_link(fn -> loop(%{}) end)
  end

  defp loop(map) do
    receive do
      {:get, key, caller} -> 
        send caller, Map.get(map, key)
        loop(map)

      {:put, key, value} ->
        loop(Map.put(map, key, value))
    end
  end
end

# To test this module, wen can open iex like:
  # - "iex ./key_value_store.ex"

# Starts a process:
  # - {:ok, pid} = KeyValueStore.start_link
  # - Process.register(pid, :kv_store_pid) # Gives a name to the pid

# And start to send messages:
  # - send pid, {:put, :name, :lucas}
  # - send pid, {:put, :age, 20}

# After send messages, we can retrieve them:
  # - send pid, {:get, :name, self()}
  # - send pid, {:get, :age, self()}

# And to actually see the messages, flush:
  # flush()