pid = spawn fn -> 1 + 2 end
# The process is already dead, to checkout we do:
IO.puts Process.alive?(pid)

# We can retrieve the PID of our current process with:
self()
IO.puts Process.alive?(self())