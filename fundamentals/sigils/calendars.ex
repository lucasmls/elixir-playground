d = ~D[2020-04-26]
IO.puts "Date: #{d}"
IO.puts "Day: #{d.day}"
IO.puts "Month: #{d.month}"
IO.puts "Year: #{d.year}"

IO.puts "---"

t = ~T[23:48:07]
IO.puts "Time: #{t}"
IO.puts "Hour: #{t.hour}"
IO.puts "Minute: #{t.minute}"
IO.puts "Second: #{t.second}"

IO.puts "---"

ndt = ~N[2020-04-26 15:51:32]
IO.puts "Native Date Time: #{ndt}"
IO.puts "Day: #{ndt.day}"
IO.puts "Month: #{ndt.month}"
IO.puts "Year: #{ndt.year}"
IO.puts "Hour: #{ndt.hour}"
IO.puts "Minute: #{ndt.minute}"
IO.puts "Second: #{ndt.second}"

IO.puts "---"

# A %DateTime{} struct contains the same fields as a NaiveDateTime with the addition of fields to track timezones.
# The ~U sigil allows developers to create a DateTime in the UTC timezone:
dt = ~U[2020-04-26 19:42:56Z]
IO.puts "Date Time: #{dt}"
%DateTime{minute: minute, time_zone: time_zone} = dt
IO.puts "Minute: #{minute}"
IO.puts "Time zone: #{time_zone}"