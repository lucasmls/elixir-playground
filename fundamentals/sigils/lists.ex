char_list = ~w(foo bar na na na)c
strings_list = ~w(foo bar na na na)s
atoms_list = ~w(foo bar na na na)a

Enum.each(char_list, fn x -> IO.puts(x) end)
IO.puts "----"
Enum.each(strings_list, fn x -> IO.puts(x) end)
IO.puts "----"
Enum.each(atoms_list, fn x -> IO.puts(x) end)