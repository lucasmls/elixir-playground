regex = ~r/foo|bar/i
# Sigils supports another eight regex different delimiters, the reason is
# to be able to write literals without escaped delimiters.
# ~r/hello/
# ~r|hello|
# ~r"hello"
# ~r'hello'
# ~r(hello)
# ~r[hello]
# ~r{hello}
# ~r<hello>

IO.puts "name" =~ regex
IO.puts "foo" =~ regex
IO.puts "bar" =~ regex