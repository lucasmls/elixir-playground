# In Elixir, it is common to loop over an Enumerable, often filtering out some results and
# mapping values into another list.
# Comprehensions are syntactic sugar for such constructs: they group those common tasks into the for special form.

# n <- [1, 2, 3, 4] is our generator
for n <- [1, 2, 3, 4] do
  IO.puts "#{n}: #{n * n}"
end

IO.puts "----"

values = [good: 1, good: 2, bad: 3, good: 4]
for {:good, n} <- values, do: IO.puts "#{n}: #{n * n}"

IO.puts "----"

multiple_of_three? = fn n -> rem(n, 3) == 0 end
for n <- 1..10, multiple_of_three?.(n), do: IO.puts "#{n}: #{n * n}"

IO.puts "----"

# We can use multiple generators

multi_gen_list = for i <- [:a, :b, :c], j <- [1, 2] do
  {i, j}
end

IO.inspect multi_gen_list

IO.puts "----"

pixels = <<213, 45, 132, 64, 76, 32, 76, 0, 0, 234, 32, 15>>
IO.puts "pixels var is binary: #{is_binary(pixels)}" # true

rgbs = for <<r::8, g::8, b::8 <- pixels>>, do: {r, g, b}
# [{213, 45, 132}, {64, 76, 32}, {76, 0, 0}, {234, 32, 15}]

IO.inspect rgbs

IO.puts "----"

string_without_spaces = for <<character <- " hello world ">>, character != ?\s, into: "" do
  <<character>>
end

IO.puts string_without_spaces

IO.puts "----"
my_map = for {key, val} <- %{"a" => 1, "b" => 2}, into: %{}, do: {key, val * val}
IO.inspect my_map