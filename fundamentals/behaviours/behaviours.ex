defmodule Parser do
  @callback parse(String.t) :: {:ok, term} | {:error, String.t}
  @callback extensions() :: [String.t]

  def parse!(implementation, contents) do
    case implementation.parse(contents) do
      {:ok, data} -> data
      {:error, error} -> raise ArgumentError, "parsing error: #{error}"
    end
  end
end

defmodule JSONParser do
  @behaviour Parser

  @impl Parser
  def parse(str), do: {:ok, "some json " <> str} # here we would implement the json parse...
  
  @impl Parser
  def extensions, do: ["json"]
end

defmodule YAMLParser do
  @behaviour Parser

  @impl Parser
  def parse(str) do
    IO.inspect binding()
    {:ok, "some yaml " <> str} # here we would implement the yaml parse...
  end
  
  @impl Parser
  def extensions, do: ["yml"]
end


IO.inspect JSONParser.parse("json string")
IO.inspect YAMLParser.parse("YAML string")