defmodule User do
  # defstruct name: "John", age: 27

  # We can define struct this way as well
  # The keys without default value must be defined first
  @enforce_keys [:name]
  defstruct [:email, name: "John", age: 27]
end

# Using the struct:
john = %User{}

# Updating a struct
lucas = %User{john | name: "Lucas"}
