try do
  exit "i'm quiting"
catch
  :exit, _ -> IO.puts "not really"
after
  IO.puts "After executed!"
end

# Note: we can wrap a whole module method in a a try clause, and enforces after, example at:
# https://elixir-lang.org/getting-started/try-catch-and-rescue.html#after