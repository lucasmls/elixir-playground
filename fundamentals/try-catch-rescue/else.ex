x = 2
try do
  1 / x
rescue
  ArithmeticError -> IO.puts :infinity
else
  y when y < 1 and y > -1 ->
    IO.puts :small
  _ ->
    IO.puts :large
end