defmodule FileSystem do
  def read() do
    File.read("./text.txt")
  end

  def write(msg) do
    {:ok, file} = File.open("./text.txt", [:write])

    IO.binwrite(file, msg)
    File.close(file)

    File.read("./text.txt")
  end
end