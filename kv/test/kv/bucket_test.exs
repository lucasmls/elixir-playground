defmodule KV.BucketTest do
  use ExUnit.Case, async: true

  setup do
    bucket = start_supervised!(KV.Bucket)
    %{bucket: bucket}
  end

  test "are temporary workers" do
    assert Supervisor.child_spec(KV.Bucket, []).restart == :temporary
  end

  test "store and retrieve values by its key", %{bucket: bucket} do
    assert KV.Bucket.get(bucket, "milk") == nil

    KV.Bucket.put(bucket, "milk", 3)
    assert KV.Bucket.get(bucket, "milk") == 3
  end

  test "deletes a value by its key", %{bucket: bucket} do
    KV.Bucket.put(bucket, "water", 10)

    assert KV.Bucket.delete(bucket, "water") == 10
    assert KV.Bucket.get(bucket, "water") == nil
  end
end
