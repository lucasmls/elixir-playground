# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :ridex,
  ecto_repos: [Ridex.Repo]

# Configures the endpoint
config :ridex, RidexWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "VB87BFAtLmZnAac3KucaUEtCmC66iZzMhrWomKCeCUMi10iUkRNUXQRk9bk9swFe",
  render_errors: [view: RidexWeb.ErrorView, accepts: ~w(html json), layout: false],
  pubsub_server: Ridex.PubSub,
  live_view: [signing_salt: "b8ZsGqg1"]

# Configures Guardian
config :ridex, Ridex.Guardian,
  issuer: "ridex",
  secret_key: "grvN1lHQ3OqlQOE+fuQ4S4r6mLC8HHB9UM+Nm2aUOCyZzNiktYdgLYPKiPXKKB2F"

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
