import { useState, useEffect } from 'react'

export const usePosition = () => {
  const [position, setPosition] = useState()

  useEffect(() => {
    const watcher = navigator.geolocation.getCurrentPosition(
      ({ coords: { latitude, longitude } }) =>
        setPosition({ lat: latitude, lng: longitude })
    )

    return () => navigator.geolocation.clearWatch(watcher)
  })

  return position
}